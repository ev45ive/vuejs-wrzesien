import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { security } from "./services/services";

security.getToken();

Vue.config.productionTip = false;

Vue.filter("shorten", (value: string, max = 20) => {
  return value.length >= max ? value.substr(0, max) + "..." : value;
});

import Card from "@/components/cards/Card.vue";
Vue.component("Card", Card);


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

declare interface Window {
  [k: string]: any;
}
(window as Window)["store"] = store;
