interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  artists?: Artist[];
  images: AlbumImage[];
}

export interface Artist extends Entity {
  followers?: {
    total: number;
  };
  images: AlbumImage[];
}

export interface AlbumImage {
  url: string;
  width?: number;
  height?: number;
}

export interface PagingObject<T> {
  items: T[];
  total: number;
}

export interface SearchResponse {
  albums?: PagingObject<Album>;
  artists?: PagingObject<Artist>;
}

/* interface Point {
  x: number;
  y: number;
}

// type Partial<T> = {
//   [k in keyof T]?: T[k]
// }

const x: Partial<Point> = {}; */
