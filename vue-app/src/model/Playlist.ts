export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  /**
   * Hex color
   */
  color: string;
  tracks?: Track[];
}

export interface Track {
  id: number;
}
