import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import PlaylistsView from "./views/PlaylistsView.vue";
import MusicSearch from "./views/MusicSearch.vue";

Vue.use(Router);
// https://github.com/vuejs/vuex-router-sync
//
// sync(store, router) // done.
// $store.router.path ...

export default new Router({
  // fallback: false,
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/playlists/:id",
      name: "Playlists2",
      component: PlaylistsView
    },
    {
      path: "/playlists",
      name: "Playlists",
      component: PlaylistsView,
      children: [
        {
          path: ":id"
        }
      ]
    },
    {
      path: "/search",
      name: "Search",
      component: MusicSearch
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "**",
      redirect: "/"
    }
  ]
});
