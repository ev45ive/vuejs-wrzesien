import { Security } from "./Security";
import { SearchResponse } from "../model/Album";

export class Search {
  constructor(private security: Security) {}

  searchAlbums(query: string) {
    return this.fetch(query, "album").then(
      resp => (resp.albums && resp.albums.items) || []
    );
  }

  searchArtists(query: string) {
    return this.fetch(query, "artist").then(
      resp => (resp.artists && resp.artists.items) || []
    );
  }

  fetch(query: string, type: string) {
    return fetch(`https://api.spotify.com/v1/search?type=${type}&q=${query}`, {
      headers: {
        Authorization: "Bearer " + this.security.getToken()
      }
    })
      .then<Response>(resp => {
        if (resp.status == 401) {
          this.security.authorize();
        }
        return resp;
      })
      .then<SearchResponse>(resp => resp.json());
  }
}

// music.searchALbums(q).
//  .then(data => console.log(data));
