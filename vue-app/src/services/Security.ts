// https://developer.spotify.com/documentation/general/guides/authorization-guide/#implicit-grant-flow

export class Security {
  constructor(
    readonly url = "https://accounts.spotify.com/authorize",
    readonly client_id = "82a120a1fd8f494e8e9d9f5cd6b7a69c",
    readonly redirect_uri = "http://localhost:8080/",
    readonly response_type = "token"
  ) {}

  authorize() {
    const url = `${this.url}?client_id=${this.client_id}&response_type=${
      this.response_type
    }&redirect_uri=${this.redirect_uri}`;

    sessionStorage.removeItem("token");
    location.replace(url);
  }

  token = "";

  getToken() {
    this.token = JSON.parse("" + sessionStorage.getItem("token"));

    if (!this.token) {
      const match = location.hash.match(/access_token=([^&]*)/);
      this.token = (match && match[1]) || "";
      location.hash = "";
      sessionStorage.setItem("token", JSON.stringify(this.token));
    }

    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
