import { Security } from "./Security";
import { Search } from "./Search";

export const security = new Security();
export const search = new Search(security);
