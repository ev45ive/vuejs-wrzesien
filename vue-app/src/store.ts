import Vue from "vue";
import Vuex from "vuex";
import counter from "./store/counter";
import playlists from "./store/playlists";
import musicsearch from '@/store/musicsearch';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    playlists,
    musicsearch
  },
  state: {},
  mutations: {},
  actions: {},
  getters: {}
});
