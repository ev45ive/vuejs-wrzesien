import { ModuleOptions, Module } from "vuex";

type State = {
  counter: number;
};

const counter: Module<State, {}> = {
  namespaced: true,
  state: {
    counter: 0
  },
  mutations: {
    increment(state, payload = 1) {
      state.counter += payload;
    },
    decrement(state, payload = 1) {
      state.counter -= payload;
    }
  },
  actions: {
    increment({ commit, state, getters }, payload) {
      commit("increment", payload);
    }
  },
  getters: {}
};

export default counter;
