import { Module } from "vuex";
import { Artist, Album } from "../model/Album";
import { search } from "@/services/services";

type Results = Album[] | Artist[];

type State = {
  results: Results;
  type: "album" | "artist";
  query: string;
};

const musicsearch: Module<State, {}> = {
  // namespaced:true,
  state: {
    query: "",
    results: [],
    type: "album"
  },
  mutations: {
    setResults(state, { results }) {
      state.results = results;
    },
    setType(state, type) {
      state.type = type;
    },
    setQuery(state, query) {
      state.query = query;
    }
  },
  getters: {
    results: state => state.results,
    query: state => state.query
  },
  actions: {
    search({ state, commit }, query) {
      commit("setQuery", query);

      switch (state.type) {
        case "album":
          search.searchAlbums(query).then((results: Album[]) => {
            commit("setResults", {
              results
            });
          });
          break;
        case "artist":
          search.searchArtists(query).then((results: Artist[]) => {
            commit("setResults", {
              results
            });
          });
          break;
      }
    }
  }
};

export default musicsearch;
