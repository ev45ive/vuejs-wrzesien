import { Module } from "vuex";
import { Playlist } from "@/model/Playlist";

type State = {
  playlists: Playlist[];
  selectedId: Playlist["id"] | null;
};

const playlists: Module<State, {}> = {
  state: {
    playlists: [
      {
        id: 123,
        name: "Vue HIts!",
        favourite: true,
        color: "#ff00ff"
      },
      {
        id: 234,
        name: "The best of Vue!",
        favourite: true,
        color: "#00ffff"
      },
      {
        id: 345,
        name: "Vue Top 20!",
        favourite: true,
        color: "#ffff00"
      }
    ],
    selectedId: null
  },
  mutations: {
    selectPlaylist(state, id: Playlist["id"]) {
      state.selectedId = id;
    },
    updatePlaylist(state, playlist: Playlist) {
      const index = state.playlists.findIndex(p => p.id == playlist.id);
      state.playlists.splice(index, 1, playlist);
    },
    addPlaylist(state, playlist: Playlist) {
      state.playlists.push(playlist);
    },
    removePlaylist(state, id: Playlist["id"]) {
      const index = state.playlists.findIndex(p => p.id == id);
      if (index !== -1) {
        state.playlists.splice(index, 1);
      }
    }
  },
  actions: {
    selectPlaylist({ state, commit }, id: Playlist["id"]) {
      commit("selectPlaylist", state.selectedId === id ? null : id);
    },
    removePlaylist({ state, commit }, id) {
      commit("removePlaylist", id);
      if (state.selectedId == id) {
        commit("selectPlaylist", null);
      }
    },
    savePlaylist({ state, commit }, playlist: Playlist) {
      const index = state.playlists.findIndex(p => p.id == playlist.id);
      if (index !== -1) {
        commit("updatePlaylist", playlist);
      } else {
        commit("addPlaylist", playlist);
      }
      commit("selectPlaylist", playlist.id);
    }
  },
  getters: {
    playlists(state) {
      return state.playlists;
    },
    playlist(state) {
      return state.playlists.find(p => p.id == state.selectedId);
    }
  }
};

export default playlists;
